package br.com.proway.trabalhopoo;

import br.com.proway.trabalhopoo.model.Biblioteca;
import br.com.proway.trabalhopoo.model.Livro;
import br.com.proway.trabalhopoo.model.Usuario;

import javax.swing.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Biblioteca biblioteca = new Biblioteca();
        for (int i = 0; i< 10; i++){
            biblioteca.insereLivro(new Livro(i+1, "Mistério v." + (i+1), "Agatha Christie" ,10));
        }

        String texto = "";
        String usuarios = "";
        String livros = "";

        String opcoes[] = {"Cadastrar Usuário","Emprestar Livro", "Devolver Livro", "Remover Usuário", "Sair"};
        int idUsuario = 1;

        while(true){
            texto = "Sistema Bib \n\n";
            texto += "Usuários \n";
            usuarios = "";
            livros = "";
            for(int i = 0; i < biblioteca.getUsuarios().size(); i++){
                usuarios += biblioteca.getUsuarios().get(i).getId() + " - Nome: " + biblioteca.getUsuarios().get(i).getNome() + "\n";
            }
            texto += usuarios + "\nLivros\n";
            for(int i = 0; i < biblioteca.getLivros().size(); i++){
                livros += biblioteca.getLivros().get(i).getId() + " - Autor: " + biblioteca.getLivros().get(i).getAutor() + " - Titulo: " + biblioteca.getLivros().get(i).getTitulo() +
                        " Quantidade: " +  biblioteca.getLivros().get(i).getQuantidade() + "\n";
            }
            texto += livros;
            int opcao = JOptionPane.showOptionDialog(null,texto,"Biblioteca",JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,null, opcoes, opcoes[0]);

            if(opcao == 0){
                String nomeUsuario = JOptionPane.showInputDialog(null, "Informe o nome do usuário: ");
                Usuario usuario = new Usuario();
                usuario.setId(idUsuario);
                usuario.setNome(nomeUsuario);
                biblioteca.insereUsuario(usuario);
                idUsuario++;
            }else if(opcao == 1){
                int id = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o id do usuário:\n\n" +usuarios ));
                int idLivro = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o id do livro:\n\n" + livros));
                Usuario usuario = biblioteca.getUsuarios().stream().filter(u -> u.getId() == id).collect(Collectors.toList()).stream().findFirst().get();
                Livro livro = biblioteca.getLivros().stream().filter(l -> l.getId() == idLivro).collect(Collectors.toList()).stream().findFirst().get();
                if(usuario.getEmprestados().stream().filter(e -> e.getId() == idLivro).collect(Collectors.toList()).stream().findFirst().isPresent()){
                    JOptionPane.showMessageDialog(null, "Não é permitido emprestar mais de uma cópia do livro.");
                }else{
                    if(livro.getQuantidade() == 0){
                        JOptionPane.showMessageDialog(null, "Livro indisponível.");
                    }else {
                        livro.setQuantidade(livro.getQuantidade() - 1);
                        usuario.inserirEmprestado(livro);
                    }
                }
            }else if(opcao == 2){
                int id = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o id do usuário:\n\n" + usuarios));
                String livrosUsuario = "";
                Usuario usuario = biblioteca.getUsuarios().stream().filter(u -> u.getId() == id).collect(Collectors.toList()).stream().findFirst().get();

                for (int i = 0; i < usuario.getEmprestados().size(); i++){
                    livrosUsuario += "Id: " + usuario.getEmprestados().get(i).getId() + " Autor: " + usuario.getEmprestados().get(i).getAutor() + " Título: " + usuario.getEmprestados().get(i).getTitulo() + "\n";
                }
                if(livrosUsuario == ""){
                    JOptionPane.showMessageDialog(null, "Usuário não tem livros emprestados.");
                }else {
                    int idLivro = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o id do livro:\n\n" + livrosUsuario));

                    Livro livro = biblioteca.getLivros().stream().filter(l -> l.getId() == idLivro).collect(Collectors.toList()).stream().findFirst().get();
                    livro.setQuantidade(livro.getQuantidade()+1);
                    usuario.removerEmprestado(idLivro);
                }
            }else if(opcao == 3){
                int id = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o id do usuário:\n\n" + usuarios));
                Usuario usuario = biblioteca.getUsuarios().stream().filter(u -> u.getId() == id).collect(Collectors.toList()).stream().findFirst().get();
                if(usuario.getEmprestados().size() > 0) {
                    JOptionPane.showMessageDialog(null, "Impossível remover usuário com livros emprestados!");
                }else{
                    biblioteca.removeUsuario(id);
                }
            }else{
                break;
            }
        }
    }
}
