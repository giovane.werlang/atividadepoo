package br.com.proway.trabalhopoo.model;

import java.util.ArrayList;

public class Biblioteca {
    private ArrayList<Livro> livros = new ArrayList<>();
    private ArrayList<Usuario> usuarios = new ArrayList<>();

    public ArrayList<Livro> getLivros() {
        return livros;
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void insereLivro(Livro livro){
        livros.add(livro);
    }

    public void removeLivro(int id){
        livros.removeIf(l -> l.getId() == id);
    }

    public void insereUsuario(Usuario usuario){
        usuarios.add(usuario);
    }

    public void removeUsuario(int id){
        usuarios.removeIf(u -> u.getId() == id);
    }
}
