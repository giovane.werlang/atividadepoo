package br.com.proway.trabalhopoo.model;

import java.util.ArrayList;

public class Usuario extends Pessoa{
    private ArrayList<Livro> emprestados = new ArrayList<>();

    public ArrayList<Livro> getEmprestados(){
        return emprestados;
    }

    public void inserirEmprestado(Livro livro){
        emprestados.add(livro);
    }

    public void removerEmprestado(int id){
        emprestados.removeIf(l -> l.getId() == id);
    }
}
